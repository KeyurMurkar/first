import React from 'react'
import Navbar from './components/Navbar'
import TextForm from './components/TextForm'
import UseEffect1 from './components/UseEffect1'
import UseEffect2 from './components/UseEffect2'
import UseMemo from './components/UseMemo'
import UseReducer from './components/UseReducer'
import UseRef from './components/UseRef'

export default function App() {
  return (
    <>
    {/* <Navbar/> */}
      <Navbar title="TextUtils" aboutText="About TextUtils" contact="Contact TextUtils"/>
      
      {/* <div className="container my-3">
      <TextForm heading="Enter the text to analyze below"/>
      </div> */}

      {/* <UseEffect1/> */}
      {/* <UseEffect2/> */}

      {/* <UseMemo/> */}

      {/* <UseReducer/> */}

      {/* <UseRef/> */}


    </>
  )
}