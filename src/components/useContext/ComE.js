import React from 'react'
import { useContext } from 'react'
import ComF from './ComF'

import { UserContext } from './UseContext'
import { LanContext } from './UseContext'

const ComE = () => {

  const user = useContext(UserContext)
  const language = useContext(LanContext)

  return (
    <div>
      {user} - {language}
    </div>
  )
}

export default ComE
