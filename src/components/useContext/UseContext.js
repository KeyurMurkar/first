import React from 'react'
import ComC from './ComC'

export const UserContext = React.createContext()
export const LanContext = React.createContext()

const UseContext = () => {
  return (
    <div>
     <UserContext.Provider value={"Keyur"}>
      <LanContext.Provider value={"JavaScript"}>
      <ComC/>
      </LanContext.Provider>
      </UserContext.Provider>
    </div>
  )
}

export default UseContext
