import React from 'react'

import { UserContext } from './UseContext'


const ComF = () => {
  return (
    <div>
      <UserContext.Consumer>
        {
          (user) => {
            return <div>User context value {user}</div>
          }
        }
      </UserContext.Consumer>
    </div>
  )
}

export default ComF
