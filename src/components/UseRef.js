import React from 'react'
import { useRef } from 'react'
import { useEffect } from 'react'

const UseRef = () => {

  const inputRef = useRef(null)

  useEffect(()=>{
    //Focus the input element
    inputRef.current.focus()
  },[])//Leave this empty as we want to run it only once

  return (
    <>
    
      <input ref={inputRef} type="text" />

    </>
  )
}

export default UseRef
