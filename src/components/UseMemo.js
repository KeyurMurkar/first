import React from 'react'
import { useMemo } from 'react';
import { useState } from 'react'

const UseMemo = () => {

  const [counterOne, setcounterOne] = useState(0);
  const [counterTwo, setcounterTwo] = useState(0);

  const IncrementOne = ()=>{
    setcounterOne(counterOne + 1)
  }

  const IncrementTwo = ()=>{
    setcounterTwo(counterTwo + 2)
  }

  const isEven = useMemo(()=>{
    console.log("isEven running")
    let i = 0;;
    while(i<2000000000) i++;
    return counterOne%2 === 0
  },[counterOne])

  // const isEven = ()=>{ //This isEven function is affecting not only counterOne but counterTwo too that's why we will use useMemo 
  //   let i = 0;;
  //   while(i<2000000000) i++;
  //   return counterOne%2 === 0
  // }
  return (
    <>
      <button className='btn btn-primary mx-2' onClick={IncrementOne}>Counter - {counterOne}</button>
      <span>{isEven ? "Even" : "Odd"}</span>
      <button className='btn btn-primary mx-2' onClick={IncrementTwo}>Counter - {counterTwo}</button>
    </>
  )
}

export default UseMemo
