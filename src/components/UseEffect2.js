import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react'

const UseEffect2 = () => {

  const [width, setWidth] = useState(window.screen.width);

  const actualWidth = () => {
    console.log(window.innerWidth);
    setWidth(window.innerWidth);
  }

  useEffect( () => {
    window.addEventListener("resize", actualWidth);
    console.log("add event");

    return () => {
      window.removeEventListener("resize", actualWidth);
      console.log("remove event");
    }
  });

  return (
    <>
      <p>The actual size of the window is:</p>
      <h1>{width}</h1>
    </>
  )
}

export default UseEffect2
