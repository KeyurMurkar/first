import React from 'react'
import { useReducer } from 'react'

const initialState = 0;

const reducer = (state, action)=>{
  console.log(state, action)
  if(action.type === "Increment"){
    return state + 1;
  }
  if(action.type === "Decrement"){
    return state - 1;
  }
  return state;
}

const UseReducer = () => {

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <>
      <p>{state}</p>
      <div className="container">
        <button className='btn btn-primary mx-2' onClick={() => dispatch({type: "Increment"})}>Inc</button>
        <button className='btn btn-primary mx-2' onClick={() => dispatch({type: "Decrement"})}>Dec</button>
      </div>
    </>
  )
}

export default UseReducer