import React, {useEffect} from 'react'
import { useState } from 'react';

const UseEffect1 = () => {

  const [count, setCount] = useState(0);

  const handleCount = ()=>{
    setCount(count+1);
  }

  useEffect(() => {
    console.log("Hello this is useEffect");
    document.title = `Chats (${count})`
  });

  return (
    <>
    <div className="container">
     <h1>{count}</h1> 
     <button className='btn btn-primary' onClick={handleCount}>Click</button>
     </div>
    </>
  )
}

export default UseEffect1