import React, {useState} from 'react'

export default function TextForm(props) {
  const handleUpClick  = () => {
    // console.log("Uppercase clicked" + text)
    let newText = text.toUpperCase();
    setText(newText)
  }
  const handleLoClick  = () => {
    // console.log("Uppercase clicked" + text)
    let newText = text.toLowerCase();
    setText(newText)
  }
  const handleOnChange  = (e) => {
    // console.log("OnChange clicked")
    setText(e.target.value)
  }
  const handleClClick = ()=>{
    let newText = "";
    setText(newText)
  }
  const handleCpClick = ()=>{
    console.log("I am copy");
    let text = document.getElementById("myBox");
    text.select();
    navigator.clipboard.writeText(text.value);
  }
  const handleExClick = ()=>{
    let newText = text.split(/[ ]+/);
    setText(newText.join(" "));
  }
  const [text, setText] = useState('');
  
  return (
    <>
    <div className="container">
    <h1>{props.heading}</h1>
      <div className="mb-3">
        <textarea className="form-control" value={text} onChange={handleOnChange} id="myBox" rows="8"></textarea>
      </div>
      <button className='btn btn-primary mx-2' onClick={handleUpClick}>Convert to Uppercase</button>
      <button className='btn btn-primary mx-2' onClick={handleLoClick}>Convert to Lowercase</button>
      <button className='btn btn-primary mx-2' onClick={handleClClick}>Clear Text</button>
      <button className='btn btn-primary mx-2' onClick={handleCpClick}>Copy Text</button>
      <button className='btn btn-primary mx-2' onClick={handleExClick}>Remove Extra Spaces</button>
      </div>
      <div className="container my-3">
        <h2>Your text summary</h2>
        <p>{text.split(" ").length} words and {text.length} characters</p>
        <p>{0.008 * text.split(" ").length} Minutes to read</p>
        <h2>Preview</h2>
        <p>{text}</p>
      </div>
    </>
  )
}
